package org.academiadecodigo.ramsters.frontrowgame.gameobject.enemies;

import org.academiadecodigo.ramsters.frontrowgame.Game;
import org.academiadecodigo.ramsters.frontrowgame.gameobject.GameObject;
import org.academiadecodigo.ramsters.frontrowgame.gameobject.NPCtype;
import org.academiadecodigo.ramsters.frontrowgame.gameobject.Position;
import org.academiadecodigo.ramsters.frontrowgame.gameobject.objects.Doors;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Benny extends NPC {
    private Doors door;

    public Benny(int x, int y, Picture picture) {
        super(new Position(x,y,picture.getWidth(),picture.getHeight()), NPCtype.BENNY, picture);
        door = new Doors(810, 238, new Picture(810, 238, "resources/images/doorhorizontal.png"));
    }

    public void move(Position pos) {
        super.move(pos, 362, 1125, 10, 234);
    }

    @Override
    public void die() {
        super.die();
        door.hide();
        GameObject.getLinkedList().remove(door);
    }

    @Override
    public void attack() {
        if (Math.random() < 0.2) {
            Game.getPlayerOne().damageTake(this.getStrenght(), NPCtype.BENNY);
        }
    }
}
