package org.academiadecodigo.ramsters.frontrowgame.gameobject.enemies;

import org.academiadecodigo.ramsters.frontrowgame.Game;
import org.academiadecodigo.ramsters.frontrowgame.gameobject.GameObject;
import org.academiadecodigo.ramsters.frontrowgame.gameobject.NPCtype;
import org.academiadecodigo.ramsters.frontrowgame.gameobject.Position;
import org.academiadecodigo.ramsters.frontrowgame.gameobject.objects.Doors;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Paulo extends NPC {

            private Doors door;


    public Paulo(int x, int y, Picture picture) {
        super(new Position(x, y, picture.getWidth(), picture.getHeight()), NPCtype.PAULO, picture);
        door = new Doors(398, 620, new Picture(398, 620, "resources/images/doorvert.png"));
    }


    @Override
    public void attack() {
        if (Math.random() < 0.4) {
            Game.getPlayerOne().damageTake(this.getStrenght(), NPCtype.PAULO);
        }
    }

    @Override
    public void die() {
        super.die();
        door.hide();
        GameObject.getLinkedList().remove(door);
    }

    public void move(Position pos) {
        super.move(pos, 0, 394, 338, 825, 4);
    }

}
